"""
Импортируем библиотечечки
"""

import numpy as np
import pandas as pd
import os
import sys
import zipfile
import math
import matplotlib.pyplot as plt
import glob

from skimage.exposure import equalize_hist
from skimage.filters import sobel
from skimage.morphology import watershed
from skimage import measure
from skimage import filters
from PIL import Image
from google.colab import drive
from datetime import datetime
from itertools import cycle
from itertools import islice, chain
from keras import Input
from keras.applications.inception_resnet_v2 import InceptionResNetV2, preprocess_input
from keras.layers import Dense, Flatten, Dropout, AveragePooling2D, concatenate
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from keras.models import Model, Sequential
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from keras.optimizers import SGD, Adam
from keras.metrics import mean_absolute_error

def flow_from_dataframe(img_data_gen, in_df, path_col, y_col, **dflow_args):
    base_dir = os.path.dirname(in_df[path_col].values[0])
    df_gen = img_data_gen.flow_from_directory(base_dir, class_mode='sparse', **dflow_args)
    df_gen.filenames = in_df[path_col].values
    df_gen.classes = np.stack(in_df[y_col].values)
    df_gen.samples = in_df.shape[0]
    df_gen.n = in_df.shape[0]
    df_gen._set_index_array()
    df_gen.directory = base_dir 
    df_gen.filepaths.extend(df_gen.filenames)
    return df_gen

NUM_EPOCHS = 250
LEARNING_RATE = 0.001
BATCH_SIZE_TRAIN = 16
BATCH_SIZE_VAL = 16

IMG_SIZE = (299, 299)

CROP = 50
SIZE = 512, 512
DIRECTORY = './boneage-training-dataset/boneage-training-dataset/'
SAVE = './rsna-bone-age/boneage-training-dataset/boneage-training-dataset/'

print('start processing...')
ctr = 0
for filename in glob.glob(DIRECTORY + '*.png'):  
    im = Image.open(filename)
    M, N = im.size
    im_s = im.crop((CROP, CROP, M - CROP, N - CROP))
    im_s.thumbnail(SIZE)
    im_s = np.array(im_s)

    markers = np.zeros_like(im_s)
    markers[im_s < im_s.mean()] = 1
    markers[im_s > im_s.mean()] = 2
    elevation_map = sobel(im_s)
    segmentation = watershed(elevation_map, markers)

    segmentation = filters.gaussian(segmentation, sigma=4)
    blobs = segmentation > segmentation.mean()
    labels, num = measure.label(blobs, return_num=True)

    max_i = 0
    max_size = 0
    for i in range(1, num + 1):
        count = np.count_nonzero(labels == i)
        if count > max_size:
            max_size = count
            max_i = i
    labels[labels != max_i] = 0
    labels[labels == max_i] = 1

    hand = np.multiply(im_s, labels)

    hand = np.pad(hand, ((math.ceil((SIZE[0] - hand.shape[0]) / 2), math.floor((SIZE[0] - hand.shape[0]) / 2)),
                         (math.ceil((SIZE[1] - hand.shape[1]) / 2), math.floor((SIZE[1] - hand.shape[1]) / 2))),
                  'constant')
    hand_img = Image.fromarray(np.uint8(hand))
    hand_img.save(SAVE + os.path.basename(filename))
    ctr += 1
    print(f'image #{ctr} saved')


core_idg = ImageDataGenerator(zoom_range=0.2,
                              fill_mode='nearest',
                              featurewise_center=False,  
                              samplewise_center=False,  
                              featurewise_std_normalization=False,
                              samplewise_std_normalization=False, 
                              zca_whitening=False,  
                              rotation_range=25,  
                              width_shift_range=0.2,  
                              height_shift_range=0.2, 
                              horizontal_flip=True,  
                              vertical_flip=False)

val_idg = ImageDataGenerator(width_shift_range=0.25, height_shift_range=0.25, horizontal_flip=True)

class_str_col = 'boneage'
gender_str_col = 'male'

base_bone_dir = os.path.join('rsna-bone-age')
boneage_df = pd.read_csv('boneage-training-dataset.csv')
boneage_df['path'] = boneage_df['id'].map(lambda x: os.path.join(base_bone_dir,
                                                         'boneage-training-dataset', 
                                                         'boneage-training-dataset', 
                                                         '{}.png'.format(x)))

boneage_df['exists'] = boneage_df['path'].map(os.path.exists)

boneage_df[gender_str_col] = boneage_df[gender_str_col].map(lambda x: np.array([1]) if x else np.array([0])) 

train_df_boneage, valid_df_boneage = train_test_split(boneage_df, test_size=0.2,
                                                      random_state=2018)  

train_gen_boneage = flow_from_dataframe(core_idg, train_df_boneage, path_col='path', y_col=class_str_col,
                                        target_size=IMG_SIZE,
                                        color_mode='rgb', batch_size=BATCH_SIZE_TRAIN)


valid_gen_boneage = flow_from_dataframe(core_idg, valid_df_boneage, path_col='path', y_col=class_str_col,
                                        target_size=IMG_SIZE,
                                        color_mode='rgb',
                                        batch_size=BATCH_SIZE_VAL) 

i1 = Input(shape=(299, 299, 3), name='input_img')
i2 = Input(shape=(1,), name='input_gender')
base = InceptionV3(input_tensor=i1, input_shape=(299, 299, 3), include_top=False, weights=None)

feature_img = base.get_layer(name='mixed10').output
feature_img = AveragePooling2D((2, 2))(feature_img)
feature_img = Flatten()(feature_img)
feature_gender = Dense(32, activation='relu')(i2)
feature = concatenate([feature_img, feature_gender], axis=1)

o = Dense(1000, activation='relu')(feature)
o = Dense(1000, activation='relu')(o)
o = Dense(1)(o)
model = Model(inputs=[i1, i2], outputs=o)
optimizer = Adam(lr=1e-3)
model.compile(loss='mean_absolute_error', optimizer=optimizer, metrics=['mae'])

def combined_generators(image_generator, gender_data, batch_size):
    gender_generator = cycle(batch(gender_data, batch_size))
    while True:
        nextImage = next(image_generator)
        nextGender = next(gender_generator)
        assert len(nextImage[0]) == len(nextGender)
        yield [nextImage[0], nextGender], nextImage[1]


def batch(iterable, n=1):
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]


model.summary()

weight_path = "{}_weights.best.hdf5".format('bone_age')

early = EarlyStopping(monitor="val_loss", mode="min",
                      patience=10)

reduceLROnPlat = ReduceLROnPlateau(monitor='val_loss', factor=0.8, patience=15, verbose=1,
                                   save_best_only=True, mode='auto', epsilon=0.0001, cooldown=5)

train_gen_wrapper = combined_generators(train_gen_boneage, train_df_boneage[gender_str_col], BATCH_SIZE_TRAIN)
val_gen_wrapper = combined_generators(valid_gen_boneage, valid_df_boneage[gender_str_col], BATCH_SIZE_VAL)

history = model.fit(train_gen_wrapper, validation_data=val_gen_wrapper,
                              epochs=NUM_EPOCHS, steps_per_epoch=len(train_gen_boneage),
                              validation_steps=len(valid_gen_boneage), 
                              callbacks=[early, reduceLROnPlat])

model.save('saved_model.h5')

train_df = pd.read_csv('drive/MyDrive/graduation/boneage-training-dataset.csv')

train_df['id'] = train_df['id'].apply(lambda x: str(x)+'.png')

train_df.head()

train_df['Мужской'] = train_df['male']

train_df['gender'] = train_df['male'].apply(lambda x: 'male' if x else 'female')
print(train_df['gender'].value_counts())
train_df['Пол'] = train_df['gender']
train_df['Пол'] = train_df['Пол'].apply(lambda x: 'Мужской' if x == 'male' else 'Женский')
sns.countplot(x = train_df['Пол'])

print('MAX age: ' + str(train_df['boneage'].max()) + ' months')

print('MIN age: ' + str(train_df['boneage'].min()) + ' months')

mean_bone_age = train_df['boneage'].mean()
print('mean: ' + str(mean_bone_age))

print('median: ' +str(train_df['boneage'].median()))

std_bone_age = train_df['boneage'].std()

train_df['bone_age_z'] = (train_df['boneage'] - mean_bone_age)/(std_bone_age)

print(train_df.head())

df_train, df_valid = train_test_split(train_df, test_size = 0.2, random_state = 0)

train_df['boneage'].hist(color = 'green')
plt.xlabel('Возраст в годах')
plt.ylabel('Количество людей')
plt.title('Количество людей в возрастной группе')

train_df['bone_age_z'].hist(color = 'violet')
plt.xlabel('Нормализованное значение')
plt.ylabel('Количество людей')
plt.title('Зависимость между количеством людей и нормализованным значением')

male = train_df[train_df['gender'] == 'male']
female = train_df[train_df['gender'] == 'female']
fig, ax = plt.subplots(2,1)
ax[0].hist(male['boneage'], color = 'blue')
ax[0].set_ylabel('Количество мужчин')
ax[1].hist(female['boneage'], color = 'red')
ax[1].set_xlabel('Возраст в месяцах')
ax[1].set_ylabel('Количество женщин')
fig.set_size_inches((10,7))

train_df['Пол'] = train_df['gender']
train_df['Костный возраст'] = train_df['boneage']
sns.swarmplot(x = train_df['Пол'], y = train_df['Костный возраст'])

male = train_df[train_df['gender'] == 'male']
female = train_df[train_df['gender'] == 'female']
fig, ax = plt.subplots(2,1)
ax[0].hist(male['boneage'], color = 'blue')
ax[0].set_ylabel('Количество мужчин')
ax[1].hist(female['boneage'], color = 'red')
ax[1].set_xlabel('Возраст в месяцах')
ax[1].set_ylabel('Количество женщин')
fig.set_size_inches((10,7))

train_df['yboneage'] = train_df['boneage'] / 12
male = train_df[train_df['gender'] == 'male']
female = train_df[train_df['gender'] == 'female']
fig, ax = plt.subplots(2,1)
ax[0].hist(male['yboneage'], color = 'seagreen', bins=20)
ax[0].set_ylabel('Количество мужчин')
ax[0].grid(True)
ax[1].hist(female['yboneage'], color = 'slateblue', bins=20)
ax[1].set_xlabel('Возраст в месяцах')
ax[1].set_ylabel('Количество женщин')
ax[1].grid(True)
fig.set_size_inches((10, 7))

import matplotlib.image as mpimg
for filename, boneage, gender in train_df[['id','boneage','gender']].sample(4).values:
    img = mpimg.imread('rsna-bone-age/boneage-training-dataset/boneage-training-dataset/'+ filename)
    plt.imshow(img)
    plt.title('Image name:{}  Bone age: {} years  Gender: {}'.format(filename, boneage/12, gender))
    plt.axis('off')
    plt.show()

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from  keras.applications.xception import preprocess_input 

img_size = 256

train_data_generator = ImageDataGenerator(preprocessing_function = preprocess_input)
val_data_generator = ImageDataGenerator(preprocessing_function = preprocess_input)

train_generator = train_data_generator.flow_from_dataframe(
    dataframe = df_train,
    directory = 'rsna-bone-age/boneage-training-dataset/boneage-training-dataset',
    x_col= 'id',
    y_col= 'bone_age_z',
    batch_size = 32,
    seed = 42,
    shuffle = True,
    class_mode= 'raw',
    flip_vertical = True,
    color_mode = 'rgb',
    target_size = (img_size, img_size))

val_generator = val_data_generator.flow_from_dataframe(
    dataframe = df_valid,
    directory = 'rsna-bone-age/boneage-training-dataset/boneage-training-dataset',
    x_col = 'id',
    y_col = 'bone_age_z',
    batch_size = 32,
    seed = 42,
    shuffle = True,
    class_mode = 'raw',
    flip_vertical = True,
    color_mode = 'rgb',
    target_size = (img_size, img_size))

test_X, test_Y = next(val_data_generator.flow_from_dataframe( 
                            df_valid, 
                            directory = 'rsna-bone-age/boneage-training-dataset/boneage-training-dataset',
                            x_col = 'id',
                            y_col = 'bone_age_z', 
                            target_size = (img_size, img_size),
                            batch_size = 2523,
                            class_mode = 'raw'
                            ))

def plot_it(history):
    fig, ax = plt.subplots( figsize=(20,10))
    ax.plot(history.history['mae_in_months'])
    ax.plot(history.history['val_mae_in_months'])
    plt.title('Model Error')
    plt.ylabel('error')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper right')
    ax.grid(color='black')
    plt.show()

from keras.metrics import mean_absolute_error
def mae_in_months(x_p, y_p):
    '''function to return mae in months'''
    return mean_absolute_error((std_bone_age*x_p + mean_bone_age), (std_bone_age*y_p + mean_bone_age))

from tensorflow.keras.layers import GlobalMaxPooling2D, Dense,Flatten
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint,EarlyStopping,ReduceLROnPlateau
from tensorflow.keras import Sequential

model_1 = tf.keras.applications.xception.Xception(input_shape = (img_size, img_size, 3),
                                           include_top = False,
                                           weights = 'imagenet')
model_1.trainable = True
model_2 = Sequential()
model_2.add(model_1)
model_2.add(GlobalMaxPooling2D())
model_2.add(Flatten())
model_2.add(Dense(10, activation = 'relu'))
model_2.add(Dense(1, activation = 'linear'))


model_2.compile(loss ='mse', optimizer= 'adam', metrics = [mae_in_months] )

model_2.summary()

early_stopping = EarlyStopping(monitor='val_loss',
                              min_delta=0,
                              patience= 5,
                              verbose=0, mode='auto')

mc = ModelCheckpoint('best_model.h5', monitor='val_loss', mode='min', save_best_only=True)

red_lr_plat = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=10, verbose=0, mode='auto', min_delta=0.0001, cooldown=0, min_lr=0)

callbacks = [early_stopping,mc, red_lr_plat]


history = model_2.fit_generator(train_generator,
                            steps_per_epoch = 315,
                            validation_data = val_generator,
                            validation_steps = 1,
                            epochs = 50,
                            callbacks= callbacks)
history
plot_it(history)